## mysql database
1. create new database `CREATE DATABASE orders_management;`

## orders-application (8000)
1. Change origin url for web **OrderOperationsPortal** at root index.js (eg: http://localhost:8001)
2. Change Database Connection at app/config/db.config.js
3. Change url for api **payment-application** at app/controllers/order.controller.js (eg: http://localhost:8002/api/payment)
4. Run command `node index.js` at root directory

## payment-application (8002)
1. Change origin url for api **orders-application** at root index.js (eg: http://localhost:8000)
2. Run command `node index.js` at root directory

## OrderOperationsPortal (8001)
1. Change base url for api **orders-application** at src/app/services/order.service.ts (eg: http://localhost:8000/api/orders)
2. Run command `ng serve --port 8001` at root directory
