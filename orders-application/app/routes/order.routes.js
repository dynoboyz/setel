module.exports = app => {
    const orders = require("../controllers/order.controller.js");
  
    var router = require("express").Router();
  
    // Create an Order
    router.post("/", orders.create);
  
    // Retrieve all Orders
    router.get("/", orders.getAll);
  
    // Retrieve a single Order with id
    router.get("/:id", orders.findOne);
  
    // Update a Order with id
    router.put("/:id", orders.update);

    app.use('/api/orders', router);
  };
  