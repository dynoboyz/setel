module.exports = {
    HOST: "localhost",
    USER: "root",
    PASSWORD: "root@123",
    DB: "orders_management",
    dialect: "mysql",
    pool: {
        max: 3,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
};