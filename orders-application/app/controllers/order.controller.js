const db = require("../models");
const Order = db.order;
const request = require('request');

exports.create = (req, res) => {
    if (!req.body.item) {
        res.status(400).send({ message: "Item can not be empty!" });
        return;
    }
  
    const order = { item: req.body.item, status: "created" };
  
    Order.create(order)
        .then(data => { 
            res.send(data);

            // Send to Payment API & update status
            let req = request.defaults({
                method: 'POST',
                headers: {
                    'x-access-token': 's3t31',
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'User-Agent': 'my-setel-client'
                },
                form: { id: data.id }
            });
            
            req('http://localhost:8002/api/payment', function(err, res, body) {
                if (res && res.statusCode == 200) {
                    console.log(`Payment for Order with id=${data.id} was ${JSON.parse(body).status}`);
                    if (JSON.parse(body).status == 'confirmed') {
                        // Confirmed ⇒ moves order to the confirmed state --(few seconds)--> delivered
                        setTimeout(function() {
                            Order.update({status: 'delivered'}, { where: { id: data.id } })
                            .then(num => {
                                if (num == 1) {
                                    console.log(`Order with id=${data.id} was updated to Delivered.`);
                                } else {
                                    console.log(`Cannot update Order with id=${data.id}. Maybe Order was not found!`);
                                }
                            })
                            .catch(err => { console.log(`Error updating Order with id=${data.id} : ${err.message}`); });
                        }, 5000);
                    } else if (JSON.parse(body).status == 'declined') {
                        // Declined ⇒ moves order to the cancelled state
                        Order.update({status: 'cancelled'}, { where: { id: data.id } })
                            .then(num => {
                                if (num == 1) {
                                    console.log("Order was with id=${id} updated to Cancelled.");
                                } else {
                                    console.log(`Cannot update Order with id=${data.id}. Maybe Order was not found!`);
                                }
                            })
                            .catch(err => { console.log(`Error updating Order with id=${data.id} : ${err.message}`); });
                    } else {
                        console.log("Error cause by status not found!");
                    }
                } else {
                    console.log(`Error response for Order with id=${data.id} : ${err.message}`);
                }
            });

        })
        .catch(err => { res.status(500).send({
                message: err.message || "Some error occurred while creating the Order."
            });
        });
};

exports.getAll = (_, res) => {
    Order.findAll({ order: [['updatedAt', 'DESC']] })
        .then(data => { res.send(data); })
        .catch(err => { res.status(500).send({
                message: err.message || "Some error occurred while retrieving orders."
            });
        });
};

exports.findOne = (req, res) => {
    const id = req.params.id;
  
    Order.findByPk(id)
        .then(data => { res.send(data); })
        .catch(err => { res.status(500).send({
                message: `Error retrieving Order with id=${id} : ${err.message}`
            });
        });
};

exports.update = (req, res) => {
    const id = req.params.id;
  
    Order.update(req.body, { where: { id: id } })
        .then(num => {
            if (num == 1) {
                res.send({ message: "Order was updated successfully." });
            } else {
                res.send({ message: `Cannot update Order with id=${id}. Maybe Order was not found or req.body is empty!` });
            }
        })
        .catch(err => { res.status(500).send({ message: `Error updating Order with id=${id} : ${err.message}` }); });
};
  