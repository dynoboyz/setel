module.exports = (sequelize, Sequelize) => {
    const Order = sequelize.define("order", {
        item: { type: Sequelize.STRING },
        status: { type: Sequelize.STRING }
    });
  
    return Order;
};