const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

// Rest apis & express middleware to enable CORS
const app = express();
var corsOptions = {
    origin: "http://localhost:8001"
};
app.use(cors(corsOptions));

// Initialize sequelize 
const db = require("./app/models");
db.sequelize.sync();

// TODO use for recreate db
// db.sequelize.sync({ force: true }).then(() => {
//     console.log("Drop and re-sync db.");
// });

// Parse the request & create body object
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Main route
app.get("/", (req, res) => {
    res.json({ message: "Welcome to Orders Application." });
});

// Include routes
require("./app/routes/order.routes")(app);

// Port & listen requests
const PORT = process.env.PORT || 8000;
app.listen(PORT, () => {
    console.log(`Orders Server is running on port ${PORT}.`);
});