import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Add 3 main routes
import { AddOrderComponent } from './components/add-order/add-order.component';
import { OrdersListComponent } from './components/orders-list/orders-list.component';
import { OrderDetailsComponent } from './components/order-details/order-details.component';

const routes: Routes = [
    { path: '', redirectTo: 'orders', pathMatch: 'full' },
    { path: 'add', component: AddOrderComponent },
    { path: 'orders', component: OrdersListComponent },
    { path: 'orders/:id', component: OrderDetailsComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }