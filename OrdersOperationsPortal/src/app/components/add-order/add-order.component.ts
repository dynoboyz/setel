import { Component, OnInit } from '@angular/core';
import { OrderService } from 'src/app/services/order.service';

@Component({
    selector: 'app-add-order',
    templateUrl: './add-order.component.html',
    styleUrls: ['./add-order.component.css']
})
export class AddOrderComponent implements OnInit {
    error_message: null;
    order = { item: '' };
    submitted = false;
    constructor(private orderService: OrderService) { }

    ngOnInit(): void {}

    saveOrder(): void {
    const data = { item: this.order.item };

    this.orderService.create(data)
        .subscribe(
        response => {
            console.log(response);
            this.submitted = true;
            this.error_message = null;
        },
        error => { this.error_message = error.message; });
    }

    newOrder(): void {
        this.error_message = null;
        this.submitted = false;
        this.order = { item: '' };
    }
}