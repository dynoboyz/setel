import { Component, OnInit } from '@angular/core';
import { OrderService } from 'src/app/services/order.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'app-order-details',
    templateUrl: './order-details.component.html',
    styleUrls: ['./order-details.component.css']
})
export class OrderDetailsComponent implements OnInit {
    currentOrder = null;
    message = '';

    constructor(
        private orderService: OrderService,
        private route: ActivatedRoute,
        private router: Router) { }

    ngOnInit(): void {
        this.message = '';
        this.getOrder(Number(this.route.snapshot.paramMap.get('id')));
    }

    getOrder(id: number): void {
        this.orderService.get(id)
            .subscribe(
                data => {
                    this.currentOrder = data;
                    console.log(data);
                },
                error => { console.log(error); });
    }

    cancelOrder(): void {
        if (confirm("Are you sure to cancel this " + this.currentOrder.item)) {
            const status = 'cancelled';
            const data = { item: this.currentOrder.item, status: status };
    
            this.orderService.update(this.currentOrder.id, data)
                .subscribe(
                    response => {
                        this.currentOrder.status = status;
                        console.log(response);
                    },
                    error => { console.log(error); });
        }
    }
}