import { Component, OnInit } from '@angular/core';
import { OrderService } from 'src/app/services/order.service';

@Component({
    selector: 'app-orders-list',
    templateUrl: './orders-list.component.html',
    styleUrls: ['./orders-list.component.css']
})
export class OrdersListComponent implements OnInit {
    error_message: null;
    orders: any;
    currentOrder = null;
    currentIndex = -1;
    item = '';

    constructor(private orderService: OrderService) { }

    ngOnInit(): void { this.retrieveOrders(); }

    retrieveOrders(): void {
    this.orderService.getAll()
        .subscribe(
        data => {
            this.orders = data;
            console.log("retrieveOrders: " + data);
            this.error_message = null;
        },
        error => { this.error_message = error.message; });
    }

    refreshList(): void {
        this.error_message = null;
        this.retrieveOrders();
        this.currentOrder = null;
        this.currentIndex = -1;
    }

    setActiveOrder(order: any, index: number): void {
        this.currentOrder = order;
        this.currentIndex = index;
    }
}