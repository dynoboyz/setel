import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

const baseUrl = 'http://localhost:8000/api/orders';

@Injectable({
    providedIn: 'root'
})
export class OrderService {

    constructor(private http: HttpClient) { }

    getAll(): Observable<any> { return this.http.get(baseUrl); }

    get(id: number): Observable<any> { return this.http.get(`${baseUrl}/${id}`); }

    create(data: any): Observable<any> { return this.http.post(baseUrl, data); }

    update(id: number, data: any): Observable<any> { return this.http.put(`${baseUrl}/${id}`, data); }
}