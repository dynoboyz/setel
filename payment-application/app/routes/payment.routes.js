module.exports = app => {
    const payment = require("../controllers/payment.controller.js");
  
    var router = require("express").Router();
  
    // Payment processing
    router.post("/", payment.process);

    app.use('/api/payment', router);
  };
  