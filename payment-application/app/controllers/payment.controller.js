exports.process = (req, res) => {
    var token = req.headers['x-access-token'];
    if (!token) return res.status(401).send({ auth: false, message: 'No token provided.' });
    
    // Validate token
    if (token == 's3t31') {
        // Generate random status
        var values = ["declined","confirmed"], status = values[Math.floor(Math.random() * values.length)];

        if (!req.body.id) {
            res.status(400).send({ message: "Order Id can not be empty!" });
            return;
        }
        res.send({ id: req.body.id, status: status });
    }
    return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });
};
  