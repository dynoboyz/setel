const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

// Rest apis & express middleware to enable CORS
const app = express();
var corsOptions = {
    origin: "http://localhost:8000"
};
app.use(cors(corsOptions));

// Parse the request & create body object
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Main route
app.get("/", (req, res) => {
    res.json({ message: "Welcome to Payment Application." });
});

// Include routes
require("./app/routes/payment.routes")(app);

// Port & listen requests
const PORT = process.env.PORT || 8002;
app.listen(PORT, () => {
    console.log(`Payment Server is running on port ${PORT}.`);
});